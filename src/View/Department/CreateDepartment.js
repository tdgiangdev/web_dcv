import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { Paper, Button } from '@material-ui/core';

export default function CreateDeparment(props) {
  const { closeCreateView } = props;
  return (
    <React.Fragment>
      <Paper elevation={3} style={{ padding: 20 }}>
        <Typography variant="h6" gutterBottom>
          Thêm mới phòng ban
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="departmentName"
              name="departmentName"
              label="Tên phòng ban"
              fullWidth
              autoComplete="given-name"
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField required id="phone" name="phone" label="Số điện thoại" fullWidth autoComplete="family-name" />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField required id="leader" name="leader" label="Trưởng phòng" fullWidth autoComplete="family-name" />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField id="address" name="address" label="Địa chỉ" fullWidth autoComplete="shipping address-line2" />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="description"
              name="description"
              label="Mô tả"
              fullWidth
              autoComplete="shipping address-line1"
            />
          </Grid>
        </Grid>

        <Grid
          container
          xs={12}
          spacing={3}
          justify="center"
          alignItems="flex-end"
          style={{ marginTop: 20 }}
          direction="column"
        >
          <Grid item xs={12} sm={6}>
            <Button variant="contained" color="secondary" onClick={closeCreateView}>
              Hủy
            </Button>
            <Button
              onClick={closeCreateView}
              variant="contained"
              style={{ backgroundColor: 'green', marginLeft: 30, color: 'white' }}
            >
              Tạo
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </React.Fragment>
  );
}
