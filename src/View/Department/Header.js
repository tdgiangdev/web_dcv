import { useState } from 'react'
import { Grid, Button, Card } from '@material-ui/core'

import CreateDepartment from './CreateDepartment'

const Header = (props) => {

    const [isCreate, setIsCreate] = useState(false)

    const closeCreateView = () => {
        setIsCreate(false)
    }


    return (
        <div style={{ flexGrow: 1 }}>
            {isCreate ? <CreateDepartment closeCreateView={closeCreateView} /> : <Grid item xs={12} >
                <Card >
                    <Grid
                        container
                        direction="row"
                        justify="space-between"
                        alignItems="center"
                        style={{ paddingLeft: 10, paddingRight: 10 }}
                    >
                        <h3>Thêm mới phòng ban</h3>
                        <Button
                            onClick={() => setIsCreate(true)}
                            style={{ backgroundColor: '#3C8DBC', color: 'white' }}
                            variant="contained"
                        >
                            Thêm mới
                        </Button>
                    </Grid>
                </Card>
            </Grid>}


        </div>
    )
}

export default Header