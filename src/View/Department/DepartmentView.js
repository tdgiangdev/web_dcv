import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import { Button, Grid, Paper, Card, Typography } from "@material-ui/core";
import Table from "../../Component/Table/TableUser";


import Header from './Header'

import { Link } from "react-router-dom";

function createData(name, phone, Leader, Description, Members, status) {

  return { name, phone, Leader, Description, Members, status };
}

const rows = [
  createData('Kinh doanh', '0866912436', 'Nguyễn Văn A', ".......mô tả", 11, 1),
  createData('Kỹ thuật', '0866912436', 'Nguyễn Văn A', ".......mô tả", 21, 1),
  createData('Marketing', '0866912436', 'Nguyễn Văn A', ".......mô tả", 10, 1),
  createData('Nhân sự', '0866912436', 'Nguyễn Văn A', ".......mô tả", 3, 1),
  createData('Kế toán', '0866912436', 'Nguyễn Văn A', ".......mô tả", 6, 1),

];
const columns = [
  { id: 'name', label: 'Tên phòng ban', minWidth: 170 },
  {
    id: 'phone',
    label: 'Điện thoại',
    minWidth: 170,
    align: 'right',

  },
  {
    id: 'Leader',
    label: 'Trường phòng',
    minWidth: 170,
    align: 'right',

  },
  {
    id: 'Description',
    label: 'Mô tả',
    minWidth: 170,
    align: 'right',

  },
  {
    id: 'Members',
    label: 'Thành viên',
    minWidth: 170,
    align: 'right',

  },

];

const DepartmentView = (props) => {
  return <div style={{ flexGrow: 1 }}>
    <h2>Quản lý phòng ban</h2>

    <Grid container spacing={3}
    >
      <Grid item xs={12}>
        <Header />
      </Grid>
      <Grid item xs={12}>
        <Table rows={rows}
          columns={columns} />
      </Grid>


    </Grid>
  </div>;
};

export default DepartmentView;
