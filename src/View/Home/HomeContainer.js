import HomeView from "./HomeView";
import { connect } from "react-redux";

const HomeContainer = (props) => {
  console.log(props.user);
  return <HomeView />;
};

const mapStateToProps = (state) => {
  return {
    user: state.userReducer,
  };
};
export default connect(mapStateToProps, {})(HomeContainer);
