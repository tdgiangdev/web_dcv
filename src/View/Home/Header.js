import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import { Button, Grid, Paper, Card, Typography } from "@material-ui/core";

import PeopleIcon from "@material-ui/icons/People";
import DomainIcon from "@material-ui/icons/Domain";
import AssessmentIcon from "@material-ui/icons/Assessment";
import EmailIcon from "@material-ui/icons/Email";
import { Link } from "react-router-dom";

const DepartmentView = (props) => {
  return (
    <Grid container style={{ marginTop: 20 }}>
      <Grid container direction="row" spacing={3}>
        <Grid item lg={3} md={6} sm={12}>
          <Card
            style={{
              backgroundColor: "#dd4b39",
              paddingTop: 20,
            }}
          >
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
              style={{ color: "white", paddingLeft: 10, paddingRight: 10 }}
            >
              <Grid item>
                <Typography variant="h3"> 5</Typography>
                <Typography variant="p">Đơn từ</Typography>
              </Grid>
              <Grid item>
                <EmailIcon
                  style={{
                    width: 100,
                    height: 100,
                    color: "rgba(0,0,0,0.15)",
                  }}
                />
              </Grid>
            </Grid>
            <Link to="./letter" style={{ textDecoration: "none" }}>
              <Button
                fullWidth
                style={{ color: "white", backgroundColor: "#992600" }}
              >
                <Typography variant="caption">Thông tin chi tiết</Typography>
              </Button>
            </Link>
          </Card>
        </Grid>
        <Grid item lg={3} md={6} sm={12}>
          <Card
            style={{
              backgroundColor: "#f39c12",
              paddingTop: 20,
            }}
          >
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
              style={{ color: "white", paddingLeft: 10, paddingRight: 10 }}
            >
              <Grid item>
                <Typography variant="h3"> 5</Typography>
                <Typography variant="p">Phòng ban</Typography>
              </Grid>
              <Grid item>
                <DomainIcon
                  style={{
                    width: 100,
                    height: 100,
                    color: "rgba(0,0,0,0.15)",
                  }}
                />
              </Grid>
            </Grid>
            <Link to="./department" style={{ textDecoration: "none" }}>
              <Button
                fullWidth
                style={{ color: "white", backgroundColor: "#cc7a00" }}
              >
                <Typography variant="caption">Thông tin chi tiết</Typography>
              </Button>
            </Link>
          </Card>
        </Grid>
        <Grid item lg={3} md={6} sm={12}>
          <Card
            style={{
              backgroundColor: "#00c0ef",
              paddingTop: 20,
            }}
          >
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
              style={{ color: "white", paddingLeft: 10, paddingRight: 10 }}
            >
              <Grid item>
                <Typography variant="h3"> 35</Typography>
                <Typography variant="p">Nhân sự</Typography>
              </Grid>
              <Grid item>
                <PeopleIcon
                  style={{
                    width: 100,
                    height: 100,
                    color: "rgba(0,0,0,0.15)",
                  }}
                />
              </Grid>
            </Grid>
            <Link to="./employee" style={{ textDecoration: "none" }}>
              <Button
                fullWidth
                style={{ color: "white", backgroundColor: "#3c8dbc" }}
              >
                <Typography variant="caption">Thông tin chi tiết</Typography>
              </Button>
            </Link>
          </Card>
        </Grid>

        <Grid item lg={3} md={6} sm={12}>
          <Card
            style={{
              backgroundColor: "#00a65a",
              paddingTop: 20,
            }}
          >
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
              style={{ color: "white", paddingLeft: 10, paddingRight: 10 }}
            >
              <Grid item>
                <Typography variant="h3"> 25</Typography>
                <Typography variant="p">Chấm công</Typography>
              </Grid>
              <Grid item>
                <AssessmentIcon
                  style={{
                    width: 100,
                    height: 100,
                    color: "rgba(0,0,0,0.15)",
                  }}
                />
              </Grid>
            </Grid>
            <Link to="./timekeeping" style={{ textDecoration: "none" }}>
              <Button
                fullWidth
                style={{ color: "white", backgroundColor: "#00802b" }}
              >
                <Typography variant="caption">Thông tin chi tiết</Typography>
              </Button>
            </Link>
          </Card>
        </Grid>
      </Grid>
      <Grid></Grid>
    </Grid>
  );
};

export default DepartmentView;
