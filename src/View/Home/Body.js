import { Grid, Paper, Card } from "@material-ui/core";
import PiceChart from "../../Component/Chart/PiceChart";
import Calendar from '../../Component/Calendar/Calendar';
import RotateChart from '../../Component/Chart/RotateChart';
import BartChart from '../../Component/Chart/BartChart';
const dataLetter = [
  { region: "Chập nhận", val: 10 },
  { region: "Từ chối", val: 4 },
  { region: "Chưa xử lý", val: 6 },


];

const dataCheckIn = [
  { region: "Chấm công", val: 35 },
  { region: "Chưa chấm công", val: 5 },
];


const Body = (props) => {
  return (
    <Grid container spacing={3}>
      <Grid xs={12} item>
        <Grid container direction="row" spacing={3}>
          <Grid xs={12} sm={6} item>
            <PiceChart data={dataLetter} title={"Đơn từ"} />
          </Grid>
          <Grid xs={12} sm={6} item>
            <PiceChart data={dataCheckIn} title={"Chấm công"} />
          </Grid>
        </Grid>
      </Grid>
      <Grid xs={12} item>
        <Grid container xs={12} spacing={3}  >
          <Grid xs={12} sm={4} item>
            <BartChart />
          </Grid>
          <Grid xs={12} sm={8} item>
            <RotateChart />
          </Grid>

        </Grid>
      </Grid>
      <Grid xs={12} sm={12} item>
        <Calendar />
      </Grid>
    </Grid>
  );
};

export default Body;
