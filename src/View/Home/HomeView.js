import { makeStyles, useTheme } from "@material-ui/core/styles";
import React from "react";
import { Grid } from "@material-ui/core";
import Header from "./Header";
import Body from "./Body";
const HomeView = (props) => {
  return (
    <div styles={{ flexGrow: 1 }}>
      <Grid container spacing={3}>
        <Grid item sm={12}    >
          <Header />
        </Grid>
        <Grid item sm={12}   >
          <Body />
        </Grid>


      </Grid>
    </div>
  );
};

export default HomeView;
