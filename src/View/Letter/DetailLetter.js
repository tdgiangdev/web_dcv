import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import {
  Typography,
  Avatar,
  ListItemAvatar,
  ListItemText,
  Divider,
  ListItem,
  List,
  Paper,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: "block",
    wrapper: "none",
  },
}));

const DetailLetter = () => {
  const classes = useStyles();
  return (
    <Paper>
      <List className={classes.root}>
        <ListItem button alignItems="flex-start">
          <ListItemAvatar>
            <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
          </ListItemAvatar>
          <ListItemText
            primary="Trần Đức Giang"
            secondary={
              <React.Fragment>
                <div>5 phút trước</div>
                <Typography
                  component="span"
                  variant="body2"
                  color="textPrimary"
                  noWrap
                  className={classes.inline}
                >
                  Đơn xin chấm công bù
                </Typography>
                <Typography
                  component="span"
                  variant="caption"
                  color="textPrimary"
                  noWrap
                  className={classes.inline}
                  number
                >
                  — Nội dung đơn ................................chấm chấm chấm
                </Typography>
              </React.Fragment>
            }
          />
        </ListItem>
        <Divider />
        <ListItem button alignItems="flex-start">
          <ListItemAvatar>
            <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
          </ListItemAvatar>
          <ListItemText
            primary="Trần Đức Giang"
            secondary={
              <React.Fragment>
                <div>5 phút trước</div>
                <Typography
                  component="span"
                  variant="body2"
                  color="textPrimary"
                  noWrap
                  className={classes.inline}
                >
                  Đơn xin chấm công bù
                </Typography>
                <Typography
                  component="span"
                  variant="caption"
                  color="textPrimary"
                  noWrap
                  className={classes.inline}
                  number
                >
                  — Nội dung đơn ................................chấm chấm chấm
                </Typography>
              </React.Fragment>
            }
          />
        </ListItem>
        <Divider />
        <ListItem button alignItems="flex-start">
          <ListItemAvatar>
            <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
          </ListItemAvatar>
          <ListItemText
            primary="Trần Đức Giang"
            secondary={
              <React.Fragment>
                <div>5 phút trước</div>
                <Typography
                  component="span"
                  variant="body2"
                  color="textPrimary"
                  noWrap
                  className={classes.inline}
                >
                  Đơn xin chấm công bù
                </Typography>
                <Typography
                  component="span"
                  variant="caption"
                  color="textPrimary"
                  noWrap
                  className={classes.inline}
                  number
                >
                  — Nội dung đơn ................................chấm chấm chấm
                </Typography>
              </React.Fragment>
            }
          />
        </ListItem>
        <Divider />
        <ListItem button alignItems="flex-start">
          <ListItemAvatar>
            <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
          </ListItemAvatar>
          <ListItemText
            primary="Trần Đức Giang"
            secondary={
              <React.Fragment>
                <div>5 phút trước</div>
                <Typography
                  component="span"
                  variant="body2"
                  color="textPrimary"
                  noWrap
                  className={classes.inline}
                >
                  Đơn xin chấm công bù
                </Typography>
                <Typography
                  component="span"
                  variant="caption"
                  color="textPrimary"
                  noWrap
                  className={classes.inline}
                  number
                >
                  — Nội dung đơn ................................chấm chấm chấm
                </Typography>
              </React.Fragment>
            }
          />
        </ListItem>
        <Divider />
        <ListItem button alignItems="flex-start">
          <ListItemAvatar>
            <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
          </ListItemAvatar>
          <ListItemText
            primary="Trần Đức Giang"
            secondary={
              <React.Fragment>
                <div>5 phút trước</div>
                <Typography
                  component="span"
                  variant="body2"
                  color="textPrimary"
                  noWrap
                  className={classes.inline}
                >
                  Đơn xin chấm công bù— Nội dung đơn
                  ................................chấm chấm chấm
                </Typography>
              </React.Fragment>
            }
          />
        </ListItem>
        <Divider />
        <ListItem button alignItems="flex-start">
          <ListItemAvatar>
            <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
          </ListItemAvatar>
          <ListItemText
            primary="Trần Đức Giang"
            secondary={
              <React.Fragment>
                <div>5 phút trước</div>
                <Typography
                  component="span"
                  variant="body2"
                  color="textPrimary"
                  noWrap
                  className={classes.inline}
                >
                  Đơn xin chấm công bù— Nội dung đơn
                  ................................chấm chấm chấm
                </Typography>
              </React.Fragment>
            }
          />
        </ListItem>
        <Divider />
        <ListItem button alignItems="flex-start">
          <ListItemAvatar>
            <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
          </ListItemAvatar>
          <ListItemText
            primary="Trần Đức Giang"
            secondary={
              <React.Fragment>
                <div>5 phút trước</div>
                <Typography
                  component="span"
                  variant="body2"
                  color="textPrimary"
                  noWrap
                  className={classes.inline}
                >
                  Đơn xin chấm công bù— Nội dung đơn
                  ................................chấm chấm chấm
                </Typography>
              </React.Fragment>
            }
          />
        </ListItem>
        <Divider />
        <ListItem alignItems="flex-start">
          <ListItemAvatar>
            <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
          </ListItemAvatar>
          <ListItemText
            primary="Trần Đức Giang"
            secondary={
              <React.Fragment>
                <div>5 phút trước</div>
                <Typography
                  component="span"
                  variant="body2"
                  color="textPrimary"
                  noWrap
                  className={classes.inline}
                >
                  Đơn xin chấm công bù— Nội dung đơn
                  ................................chấm chấm chấm
                </Typography>
              </React.Fragment>
            }
          />
        </ListItem>
        <Divider />
        <ListItem alignItems="flex-start">
          <ListItemAvatar>
            <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
          </ListItemAvatar>
          <ListItemText
            primary="Trần Đức Giang"
            secondary={
              <React.Fragment>
                <div>5 phút trước</div>
                <Typography
                  component="span"
                  variant="body2"
                  color="textPrimary"
                  noWrap
                  className={classes.inline}
                >
                  Đơn xin chấm công bù— Nội dung đơn
                  ................................chấm chấm chấm
                </Typography>
              </React.Fragment>
            }
          />
        </ListItem>
      </List>
    </Paper>
  );
};

export default DetailLetter;
