import React from "react";
import { Grid, Paper } from "@material-ui/core";

import DetailLetter from "./DetailLetter";
import ControlLetter from "./ControlLetter";

const LetterView = (props) => {
  return (
    <React.Fragment>
      <h2>Quản lý đơn từ</h2>
      <Grid container direction="row" spacing={3}>
        <Grid item xs={12} md={3}>
          <ControlLetter />
        </Grid>
        <Grid item xs={12} md={9}>
          <DetailLetter />
        </Grid>
      </Grid>
    </React.Fragment>
  );
};

export default LetterView;
