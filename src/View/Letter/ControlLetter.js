import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  ListItemText,
  Collapse,
  ListItemIcon,
  ListItem,
  List,
  ListSubheader,
  Divider,
  Paper,
} from "@material-ui/core";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import DraftsIcon from "@material-ui/icons/Drafts";
import CachedIcon from "@material-ui/icons/Cached";
import SendIcon from "@material-ui/icons/Send";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import StarBorder from "@material-ui/icons/StarBorder";
import AnnouncementIcon from "@material-ui/icons/Announcement";
import RemoveCircleOutlineIcon from "@material-ui/icons/RemoveCircleOutline";
import HowToRegIcon from "@material-ui/icons/HowToReg";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

export default function ControlLetter() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <Paper>
      <List
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={
          <ListSubheader component="div" id="nested-list-subheader">
            Menu
          </ListSubheader>
        }
        className={classes.root}
      >
        <Divider />
        <ListItem button onClick={handleClick}>
          <ListItemIcon style={{ color: "green" }}>
            <CachedIcon />
          </ListItemIcon>
          <ListItemText primary="Đơn chưa xử lý" />
          {open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItem button className={classes.nested}>
              <ListItemIcon style={{ color: "#f39c12" }}>
                <RemoveCircleOutlineIcon />
              </ListItemIcon>
              <ListItemText primary="Nghỉ phép" />
            </ListItem>
            <ListItem button className={classes.nested}>
              <ListItemIcon style={{ color: "#00c0ef" }}>
                <HowToRegIcon />
              </ListItemIcon>
              <ListItemText primary="Chấm công bù" />
            </ListItem>
            <ListItem button className={classes.nested}>
              <ListItemIcon style={{ color: "red" }}>
                <AnnouncementIcon />
              </ListItemIcon>
              <ListItemText primary="Khiếu nại" />
            </ListItem>
          </List>
        </Collapse>
        <Divider />
        <ListItem button>
          <ListItemIcon style={{ color: "#f39c12" }}>
            <RemoveCircleOutlineIcon />
          </ListItemIcon>
          <ListItemText primary="Đơn xin nghỉ phép" />
        </ListItem>
        <Divider />
        <ListItem button>
          <ListItemIcon style={{ color: "#00c0ef" }}>
            <HowToRegIcon />
          </ListItemIcon>
          <ListItemText primary="Đơn chấm công bù" />
        </ListItem>
        <Divider />
        <ListItem button>
          <ListItemIcon style={{ color: "red" }}>
            <AnnouncementIcon />
          </ListItemIcon>
          <ListItemText primary="Đơn khiếu nại" />
        </ListItem>
      </List>
    </Paper>
  );
}
