import { useState } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import PersonIcon from '@material-ui/icons/Person';
import { Grid, Card, Paper, Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';

import CreateEmployee from './CreateEmployee';

const EmployeeView = (props) => {
  const [isCreate, setIsCreate] = useState(false);

  const openCreate = () => setIsCreate(true);
  const closeCreate = () => setIsCreate(false);

  return (
    <div style={{ flexGrow: 1 }}>
      <Grid container direction="row" alignItems="center" spacing={3}>
        <Grid item lg={3} md={6} sm={12}>
          <Card>
            <Grid container direction="row" alignItems="center" style={{ padding: 20, backgroundColor: 'white' }}>
              <PersonIcon
                style={{
                  width: 50,
                  height: 50,
                  color: 'white',
                  backgroundColor: '#00A65A',
                  marginRight: 10,
                }}
              />

              <Typography variant="subtitle2">15 Hoạt động</Typography>
            </Grid>
          </Card>
        </Grid>
        <Grid item lg={3} md={6} sm={12}>
          <Card>
            <Grid container direction="row" alignItems="center" style={{ padding: 20, backgroundColor: 'white' }}>
              <PersonIcon
                style={{
                  width: 50,
                  height: 50,
                  color: 'white',
                  backgroundColor: '#DD4B39',
                  marginRight: 10,
                }}
              />

              <Typography variant="subtitle2">5 Không hoạt động</Typography>
            </Grid>
          </Card>
        </Grid>
        <Grid item lg={3} md={6} sm={12}>
          <Card>
            <Grid container direction="row" alignItems="center" style={{ padding: 20, backgroundColor: 'white' }}>
              <PersonIcon
                style={{
                  width: 50,
                  height: 50,
                  color: 'white',
                  backgroundColor: '#b366ff',
                  marginRight: 10,
                }}
              />

              <Typography variant="subtitle2">60% Nam</Typography>
            </Grid>
          </Card>
        </Grid>
        <Grid item lg={3} md={6} sm={12}>
          <Card>
            <Grid container direction="row" alignItems="center" style={{ padding: 20, backgroundColor: 'white' }}>
              <PersonIcon
                style={{
                  width: 50,
                  height: 50,
                  color: 'white',
                  backgroundColor: '#ff8c1a',
                  marginRight: 10,
                }}
              />

              <Typography variant="subtitle2">40% Nữ</Typography>
            </Grid>
          </Card>
        </Grid>

        <Grid item xs={12}>
          {isCreate ? (
            <CreateEmployee closeCreateView={closeCreate} />
          ) : (
            <Card>
              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
                style={{ paddingLeft: 10, paddingRight: 10 }}
              >
                <h3>Thêm mới nhân viên</h3>
                <Button onClick={openCreate} style={{ backgroundColor: '#3C8DBC', color: 'white' }} variant="contained">
                  Thêm mới
                </Button>
              </Grid>
            </Card>
          )}
        </Grid>
      </Grid>
    </div>
  );
};

const styles = {
  contaienr: {
    backgroundColor: 'blue',
    color: 'yellow',
    flex: 1,
  },
};

export default EmployeeView;
