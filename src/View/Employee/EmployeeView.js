
import { makeStyles, useTheme } from '@material-ui/core/styles';

import { Grid, Card, Paper } from '@material-ui/core'
import Header from './Header';

import TableUser from '../../Component/Table/TableUser'

function createData(name, phone, email, positon, department, status) {

    return { name, phone, email, positon, department, status };
}

const rows = [
    createData('Trần Văn A', '0866912436', 'atv@dcv.vn', "Nhân viên", "Kỹ thuật", 1),
    createData('Trần Văn A', '0866912436', 'atv@dcv.vn', "Nhân viên", "Kỹ thuật", 1),
    createData('Trần Văn A', '0866912436', 'atv@dcv.vn', "Nhân viên", "Kỹ thuật", 1),
    createData('Trần Văn A', '0866912436', 'atv@dcv.vn', "Nhân viên", "Kỹ thuật", 0),
    createData('Trần Văn A', '0866912436', 'atv@dcv.vn', "Nhân viên", "Kỹ thuật", 1),
    createData('Trần Văn A', '0866912436', 'atv@dcv.vn', "Nhân viên", "Kỹ thuật", 1),
    createData('Trần Văn A', '0866912436', 'atv@dcv.vn', "Nhân viên", "Kỹ thuật", 1),
    createData('Trần Văn A', '0866912436', 'atv@dcv.vn', "Nhân viên", "Kỹ thuật", 0),
    createData('Trần Văn A', '0866912436', 'atv@dcv.vn', "Nhân viên", "Kỹ thuật", 1),
    createData('Trần Văn A', '0866912436', 'atv@dcv.vn', "Nhân viên", "Kỹ thuật", 1),
    createData('Trần Văn A', '0866912436', 'atv@dcv.vn', "Nhân viên", "Kỹ thuật", 0),
    createData('Trần Văn A', '0866912436', 'atv@dcv.vn', "Nhân viên", "Kỹ thuật", 1),
    createData('Trần Văn A', '0866912436', 'atv@dcv.vn', "Nhân viên", "Kỹ thuật", 0),
    createData('Trần Văn A', '0866912436', 'atv@dcv.vn', "Nhân viên", "Kỹ thuật", 1),
    createData('Trần Văn A', '0866912436', 'atv@dcv.vn', "Nhân viên", "Kỹ thuật", 1),
    createData('Trần Văn A', '0866912436', 'atv@dcv.vn', "Nhân viên", "Kỹ thuật", 1),
    createData('Trần Văn A', '0866912436', 'atv@dcv.vn', "Nhân viên", "Kỹ thuật", 0),

];
const columns = [
    { id: 'name', label: 'Tên', minWidth: 170 },
    {
        id: 'phone',
        label: 'Điện thoại',
        minWidth: 170,
        align: 'right',
        format: (value) => value.toLocaleString('en-US'),
    },
    {
        id: 'email',
        label: 'Email',
        minWidth: 170,
        align: 'right',
        format: (value) => value.toLocaleString('en-US'),
    },
    {
        id: 'positon',
        label: 'Chức vụ',
        minWidth: 170,
        align: 'right',
        format: (value) => value.toFixed(2),
    },
    {
        id: 'department',
        label: 'Phòng ban',
        minWidth: 170,
        align: 'right',
        format: (value) => value.toFixed(2),
    },

];


const EmployeeView = (props) => {

    return (
        <div style={{ flexGrow: 1 }}>
            <h2>Quản lý nhân viên</h2>
            <Grid container spacing={3}
            >
                <Grid item spacing={3} xs={12}
                >
                    <Header />
                </Grid>
                <Grid xs={12} item spacing={3}
                >
                    <TableUser
                        rows={rows}
                        columns={columns}
                    />
                </Grid>
            </Grid>
        </div>

    )
}



const styles = {
    contaienr: {
        backgroundColor: 'blue',
        color: 'yellow',
        flex: 1
    }
}

export default EmployeeView;