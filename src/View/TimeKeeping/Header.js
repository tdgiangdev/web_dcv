import {
  Grid,
  TextField,
  Paper,
  Button,
  Card,
  Typography,
} from "@material-ui/core";
import CalendarTodayIcon from "@material-ui/icons/CalendarToday";
import PersonIcon from "@material-ui/icons/Person";
const Header = (props) => {
  return (
    <Grid
      container
      spacing={3}
      direction="row"
      justify="space-between"
      alignItems="center"
    >
      <Grid container spacing={3} style={{ paddingLeft: 10, paddingRight: 10 }}>
        <Grid item lg={3} md={6} sm={12}>
          <Card>
            <Grid
              container
              direction="row"
              alignItems="center"
              style={{ padding: 20, backgroundColor: "white" }}
            >
              <PersonIcon
                style={{
                  width: 50,
                  height: 50,
                  color: "white",
                  backgroundColor: "#00A65A",
                  marginRight: 10,
                }}
              />

              <Typography variant="subtitle2">15 Hoạt động</Typography>
            </Grid>
          </Card>
        </Grid>
        <Grid item lg={3} md={6} sm={12}>
          <Card>
            <Grid
              container
              direction="row"
              alignItems="center"
              style={{ padding: 20, backgroundColor: "white" }}
            >
              <PersonIcon
                style={{
                  width: 50,
                  height: 50,
                  color: "white",
                  backgroundColor: "#ff8c1a",
                  marginRight: 10,
                }}
              />

              <Typography variant="subtitle2">10 chấm công hợp lệ</Typography>
            </Grid>
          </Card>
        </Grid>
        <Grid item lg={3} md={6} sm={12}>
          <Card>
            <Grid
              container
              direction="row"
              alignItems="center"
              style={{ padding: 20, backgroundColor: "white" }}
            >
              <PersonIcon
                style={{
                  width: 50,
                  height: 50,
                  color: "white",
                  backgroundColor: "#b366ff",
                  marginRight: 10,
                }}
              />

              <Typography variant="subtitle2">5 Đi muộn về sớm </Typography>
            </Grid>
          </Card>
        </Grid>
        <Grid item lg={3} md={6} sm={12}>
          <Card>
            <Grid
              container
              direction="row"
              alignItems="center"
              style={{ padding: 20, backgroundColor: "white" }}
            >
              <PersonIcon
                style={{
                  width: 50,
                  height: 50,
                  color: "white",
                  backgroundColor: "#DD4B39",
                  marginRight: 10,
                }}
              />

              <Typography variant="subtitle2">5 Vắng mặt</Typography>
            </Grid>
          </Card>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Header;
