import React from "react";
import Header from "./Header";
import { Grid, Card } from "@material-ui/core";
import TableTimeKeeping from "../../Component/Table/TableTimeKeeping";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
function createData(name, date, status, checkin, checkout, dmvs, check) {
  return { name, date, status, checkin, checkout, dmvs, check };
}

const rows = [
  createData("Trần Văn A", "07/12/2020", "Hoạt động", "08:12", "17:35", "-", 1),
  createData(
    "Trần Văn B",
    "07/12/2020",
    "Hoạt động",
    "08:12",
    "15:35",
    "02:20",
    0
  ),
  createData("Trần Văn A", "07/12/2020", "Vắng mặt", "-", "-", "-", 0),
  createData("Trần Văn V", "07/12/2020", "Hoạt động", "08:12", "17:35", "-", 1),
  createData("Trần Văn A", "07/12/2020", "Hoạt động", "08:12", "17:35", "-", 1),
  createData("Trần Văn C", "07/12/2020", "Vắng mặt", "-", "-", "-", 0),
  createData("Trần Văn A", "07/12/2020", "Vắng mặt", "-", "-", "-", 0),
  createData("Trần Văn D", "07/12/2020", "Hoạt động", "08:12", "17:35", "-", 1),
  createData("Trần Văn A", "07/12/2020", "Hoạt động", "08:12", "17:35", "-", 1),
  createData("Trần Văn A", "07/12/2020", "Hoạt động", "08:12", "17:35", "-", 1),
  createData("Trần Văn A", "07/12/2020", "Hoạt động", "08:12", "17:35", "-", 1),
  createData("Trần Văn A", "07/12/2020", "Hoạt động", "08:12", "17:35", "-", 1),
  createData("Trần Văn A", "07/12/2020", "Hoạt động", "08:12", "17:35", "-", 1),
];
const columns = [
  { id: "name", label: "Tên", minWidth: 170 },
  {
    id: "date",
    label: "Ngày",
    minWidth: 170,
    align: "right",
    format: (value) => value.toLocaleString("en-US"),
  },
  {
    id: "status",
    label: "Trạng thái",
    minWidth: 170,
    align: "right",
    format: (value) => value.toLocaleString("en-US"),
  },
  {
    id: "checkin",
    label: "Bắt đầu",
    minWidth: 170,
    align: "right",
    format: (value) => value.toFixed(2),
  },
  {
    id: "checkout",
    label: "Kết thúc",
    minWidth: 170,
    align: "right",
    format: (value) => value.toFixed(2),
  },
  {
    id: "dmvs",
    label: "Đi muộn về sớm",
    minWidth: 100,
    align: "right",
    format: (value) => value.toFixed(2),
  },
];

const TimeKeepingView = (props) => {
  const [selectedDate, setSelectedDate] = React.useState(new Date(Date.now()));

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };
  return (
    <React.Fragment>
      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
            style={{ marginTop: 10 }}
          >
            <h2>Quản lý chấm công</h2>
            <Card elevation={3}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  style={{ paddingRight: 10, paddingLeft: 20 }}
                  disableToolbar
                  variant="inline"
                  format="dd/MM/yyyy"
                  margin="normal"
                  id="date-picker-inline"
                  value={selectedDate}
                  onChange={handleDateChange}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                />
              </MuiPickersUtilsProvider>
            </Card>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Header />
        </Grid>
        <Grid item xs={12}>
          <TableTimeKeeping rows={rows} columns={columns} />
        </Grid>
      </Grid>
    </React.Fragment>
  );
};

export default TimeKeepingView;
