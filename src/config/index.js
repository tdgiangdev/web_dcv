export * from "./Function";
export * from "./Setting";

export default {
  CREATE_INVOICE: `${NetworkSetting.ROOT_MOB}/apInvoiceServiceRest/apInvoice/`,
  EDIT_INVOICE: `${NetworkSetting.ROOT_MOB}/apInvoiceServiceRest/apInvoice/update`,
};
