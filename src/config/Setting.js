export const ColorsSetting = {
  MAIN_COLOR: "#0062E1",
};
export const OneSignalKey = {
  development: "9f81dc21-7054-4f5f-ad53-87f025d2d154",
  production: "9f81dc21-7054-4f5f-ad53-87f025d2d154",
};
export const NetworkSetting = {
  // link dcv
  // ROOT_MOB: "http://222.252.22.174:8080/erp-service-mobile",
  // ROOT_WEB: "http://222.252.22.174:8080/erp-service",
  ROOT_MOB:
    "https://apiconnection.viettel.vn:8570/mobile-apis/erp/pmtc-service-mobile",
  ROOT_WEB: "https://apiconnection.viettel.vn:8570/mobile-apis/erp",

  ROOT: "http://10.10.0.14:8080",
  SOCKET: "http://203.162.10.108:3070",

  AVATAR_LINK:
    "https://apis.viettel.vn:8282/ttns/api/v2/employee/avatar-resize",
};
