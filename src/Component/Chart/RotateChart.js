import * as React from 'react';
import Paper from '@material-ui/core/Paper';
import {
    Chart,
    BarSeries,
    Title,
    ArgumentAxis,
    ValueAxis,
} from '@devexpress/dx-react-chart-material-ui';

import { Animation } from '@devexpress/dx-react-chart';

const data = [
    { month: '12', population: 35 },
    { month: '11', population: 30 },
    { month: '10', population: 36 },
    { month: '9', population: 35 },
    { month: '8', population: 35 },
    { month: '7', population: 38 },
    { month: '6', population: 34 },
    { month: '5', population: 33 },
    { month: '4', population: 31 },
    { month: '3', population: 28 },
    { month: '2', population: 33 },
    { month: '1', population: 38 },

];

export default class Demo extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            data,
        };
    }

    render() {
        const { data: chartData } = this.state;

        return (
            <Paper>
                <Chart
                    height={500}
                    data={chartData}
                    rotated
                >
                    <ArgumentAxis />
                    <ValueAxis max={7} />

                    <BarSeries
                        valueField="population"
                        argumentField="month"
                    />
                    <Title text="Số lượng nhân sự  (2020)" />
                    <Animation />
                </Chart>
            </Paper>
        );
    }
}