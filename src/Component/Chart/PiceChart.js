import * as React from "react";
import Paper from "@material-ui/core/Paper";
import {
  Chart,
  PieSeries,
  Legend,
  Tooltip,

} from "@devexpress/dx-react-chart-material-ui";
import { Typography } from "@material-ui/core";
import { Animation, Palette, HoverState, EventTracker } from "@devexpress/dx-react-chart";

const SpecialMarkerComponent = ({ name, color }) => {
  return (
    <div
      style={{
        width: 20,
        height: 20,
        borderRadius: 10,
        backgroundColor: color,
      }}
    ></div>
  );
};


export default class Demo extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      targetItem: undefined,
      hover: undefined,
    };

    this.changeTargetItem = targetItem => this.setState({ targetItem });

    this.changeHover = hover => this.setState({ hover });
  }

  render() {
    const { data, title, targetItem, hover } = this.props;

    return (
      <Paper style={{ padding: 10 }}>
        <Typography variant="h5"  >{title}</Typography>
        <Chart height={350} data={data}>
          <Palette scheme={["#9CCC65", "#FF7043", "#42A5F5"]} />
          <PieSeries
            valueField="val"
            argumentField="region"
            innerRadius={0.6}
          />
          <Legend markerComponent={SpecialMarkerComponent} />
          <Animation />
          <EventTracker />
          <Tooltip targetItem={targetItem} onTargetItemChange={this.changeTargetItem} />
          <HoverState hover={hover} onHoverChange={this.changeHover} />

        </Chart>
      </Paper>
    );
  }
}
