export default NewComponent = React.createClass({
  render: function () {
    return (
      <div>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <title>AdminLTE 2 | Mailbox</title>
        {/* Tell the browser to be responsive to screen width */}
        <meta
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
          name="viewport"
        />
        {/* Bootstrap 3.3.6 */}
        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css" />
        {/* Font Awesome */}
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"
        />
        {/* Ionicons */}
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"
        />
        {/* fullCalendar 2.2.5*/}
        <link
          rel="stylesheet"
          href="../../plugins/fullcalendar/fullcalendar.min.css"
        />
        <link
          rel="stylesheet"
          href="../../plugins/fullcalendar/fullcalendar.print.css"
          media="print"
        />
        {/* Theme style */}
        <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css" />
        {/* AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. */}
        <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css" />
        {/* iCheck */}
        <link rel="stylesheet" href="../../plugins/iCheck/flat/blue.css" />
        {/* HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries */}
        {/* WARNING: Respond.js doesn't work if you view the page via file:// */}
        {/*[if lt IE 9]>
    
    
    <![endif]*/}
        <div className="wrapper">
          <header className="main-header">
            {/* Logo */}
            <a href="../../index2.html" className="logo">
              {/* mini logo for sidebar mini 50x50 pixels */}
              <span className="logo-mini">
                <b>A</b>LT
              </span>
              {/* logo for regular state and mobile devices */}
              <span className="logo-lg">
                <b>Admin</b>LTE
              </span>
            </a>
            {/* Header Navbar: style can be found in header.less */}
            <nav className="navbar navbar-static-top">
              {/* Sidebar toggle button*/}
              <a
                href="#"
                className="sidebar-toggle"
                data-toggle="offcanvas"
                role="button"
              >
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar" />
                <span className="icon-bar" />
                <span className="icon-bar" />
              </a>
              <div className="navbar-custom-menu">
                <ul className="nav navbar-nav">
                  {/* Messages: style can be found in dropdown.less*/}
                  <li className="dropdown messages-menu">
                    <a
                      href="#"
                      className="dropdown-toggle"
                      data-toggle="dropdown"
                    >
                      <i className="fa fa-envelope-o" />
                      <span className="label label-success">4</span>
                    </a>
                    <ul className="dropdown-menu">
                      <li className="header">You have 4 messages</li>
                      <li>
                        {/* inner menu: contains the actual data */}
                        <ul className="menu">
                          <li>
                            {/* start message */}
                            <a href="#">
                              <div className="pull-left">
                                <img
                                  src="../../dist/img/user2-160x160.jpg"
                                  className="img-circle"
                                  alt="User Image"
                                />
                              </div>
                              <h4>
                                Support Team
                                <small>
                                  <i className="fa fa-clock-o" /> 5 mins
                                </small>
                              </h4>
                              <p>Why not buy a new awesome theme?</p>
                            </a>
                          </li>
                          {/* end message */}
                        </ul>
                      </li>
                      <li className="footer">
                        <a href="#">See All Messages</a>
                      </li>
                    </ul>
                  </li>
                  {/* Notifications: style can be found in dropdown.less */}
                  <li className="dropdown notifications-menu">
                    <a
                      href="#"
                      className="dropdown-toggle"
                      data-toggle="dropdown"
                    >
                      <i className="fa fa-bell-o" />
                      <span className="label label-warning">10</span>
                    </a>
                    <ul className="dropdown-menu">
                      <li className="header">You have 10 notifications</li>
                      <li>
                        {/* inner menu: contains the actual data */}
                        <ul className="menu">
                          <li>
                            <a href="#">
                              <i className="fa fa-users text-aqua" /> 5 new
                              members joined today
                            </a>
                          </li>
                        </ul>
                      </li>
                      <li className="footer">
                        <a href="#">View all</a>
                      </li>
                    </ul>
                  </li>
                  {/* Tasks: style can be found in dropdown.less */}
                  <li className="dropdown tasks-menu">
                    <a
                      href="#"
                      className="dropdown-toggle"
                      data-toggle="dropdown"
                    >
                      <i className="fa fa-flag-o" />
                      <span className="label label-danger">9</span>
                    </a>
                    <ul className="dropdown-menu">
                      <li className="header">You have 9 tasks</li>
                      <li>
                        {/* inner menu: contains the actual data */}
                        <ul className="menu">
                          <li>
                            {/* Task item */}
                            <a href="#">
                              <h3>
                                Design some buttons
                                <small className="pull-right">20%</small>
                              </h3>
                              <div className="progress xs">
                                <div
                                  className="progress-bar progress-bar-aqua"
                                  style={{ width: "20%" }}
                                  role="progressbar"
                                  aria-valuenow={20}
                                  aria-valuemin={0}
                                  aria-valuemax={100}
                                >
                                  <span className="sr-only">20% Complete</span>
                                </div>
                              </div>
                            </a>
                          </li>
                          {/* end task item */}
                        </ul>
                      </li>
                      <li className="footer">
                        <a href="#">View all tasks</a>
                      </li>
                    </ul>
                  </li>
                  {/* User Account: style can be found in dropdown.less */}
                  <li className="dropdown user user-menu">
                    <a
                      href="#"
                      className="dropdown-toggle"
                      data-toggle="dropdown"
                    >
                      <img
                        src="../../dist/img/user2-160x160.jpg"
                        className="user-image"
                        alt="User Image"
                      />
                      <span className="hidden-xs">Alexander Pierce</span>
                    </a>
                    <ul className="dropdown-menu">
                      {/* User image */}
                      <li className="user-header">
                        <img
                          src="../../dist/img/user2-160x160.jpg"
                          className="img-circle"
                          alt="User Image"
                        />
                        <p>
                          Alexander Pierce - Web Developer
                          <small>Member since Nov. 2012</small>
                        </p>
                      </li>
                      {/* Menu Body */}
                      <li className="user-body">
                        <div className="row">
                          <div className="col-xs-4 text-center">
                            <a href="#">Followers</a>
                          </div>
                          <div className="col-xs-4 text-center">
                            <a href="#">Sales</a>
                          </div>
                          <div className="col-xs-4 text-center">
                            <a href="#">Friends</a>
                          </div>
                        </div>
                        {/* /.row */}
                      </li>
                      {/* Menu Footer*/}
                      <li className="user-footer">
                        <div className="pull-left">
                          <a href="#" className="btn btn-default btn-flat">
                            Profile
                          </a>
                        </div>
                        <div className="pull-right">
                          <a href="#" className="btn btn-default btn-flat">
                            Sign out
                          </a>
                        </div>
                      </li>
                    </ul>
                  </li>
                  {/* Control Sidebar Toggle Button */}
                  <li>
                    <a href="#" data-toggle="control-sidebar">
                      <i className="fa fa-gears" />
                    </a>
                  </li>
                </ul>
              </div>
            </nav>
          </header>
          {/* Left side column. contains the logo and sidebar */}
          <aside className="main-sidebar">
            {/* sidebar: style can be found in sidebar.less */}
            <section className="sidebar">
              {/* Sidebar user panel */}
              <div className="user-panel">
                <div className="pull-left image">
                  <img
                    src="../../dist/img/user2-160x160.jpg"
                    className="img-circle"
                    alt="User Image"
                  />
                </div>
                <div className="pull-left info">
                  <p>Alexander Pierce</p>
                  <a href="#">
                    <i className="fa fa-circle text-success" /> Online
                  </a>
                </div>
              </div>
              {/* search form */}
              <form action="#" method="get" className="sidebar-form">
                <div className="input-group">
                  <input
                    type="text"
                    name="q"
                    className="form-control"
                    placeholder="Search..."
                  />
                  <span className="input-group-btn">
                    <button
                      type="submit"
                      name="search"
                      id="search-btn"
                      className="btn btn-flat"
                    >
                      <i className="fa fa-search" />
                    </button>
                  </span>
                </div>
              </form>
              {/* /.search form */}
              {/* sidebar menu: : style can be found in sidebar.less */}
              <ul className="sidebar-menu">
                <li className="header">MAIN NAVIGATION</li>
                <li className="treeview">
                  <a href="#">
                    <i className="fa fa-dashboard" /> <span>Dashboard</span>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-left pull-right" />
                    </span>
                  </a>
                  <ul className="treeview-menu">
                    <li>
                      <a href="../../index.html">
                        <i className="fa fa-circle-o" /> Dashboard v1
                      </a>
                    </li>
                    <li>
                      <a href="../../index2.html">
                        <i className="fa fa-circle-o" /> Dashboard v2
                      </a>
                    </li>
                  </ul>
                </li>
                <li className="treeview">
                  <a href="#">
                    <i className="fa fa-files-o" />
                    <span>Layout Options</span>
                    <span className="pull-right-container">
                      <span className="label label-primary pull-right">4</span>
                    </span>
                  </a>
                  <ul className="treeview-menu">
                    <li>
                      <a href="../layout/top-nav.html">
                        <i className="fa fa-circle-o" /> Top Navigation
                      </a>
                    </li>
                    <li>
                      <a href="../layout/boxed.html">
                        <i className="fa fa-circle-o" /> Boxed
                      </a>
                    </li>
                    <li>
                      <a href="../layout/fixed.html">
                        <i className="fa fa-circle-o" /> Fixed
                      </a>
                    </li>
                    <li>
                      <a href="../layout/collapsed-sidebar.html">
                        <i className="fa fa-circle-o" /> Collapsed Sidebar
                      </a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="../widgets.html">
                    <i className="fa fa-th" /> <span>Widgets</span>
                    <span className="pull-right-container">
                      <small className="label pull-right bg-green">new</small>
                    </span>
                  </a>
                </li>
                <li className="treeview">
                  <a href="#">
                    <i className="fa fa-pie-chart" />
                    <span>Charts</span>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-left pull-right" />
                    </span>
                  </a>
                  <ul className="treeview-menu">
                    <li>
                      <a href="../charts/chartjs.html">
                        <i className="fa fa-circle-o" /> ChartJS
                      </a>
                    </li>
                    <li>
                      <a href="../charts/morris.html">
                        <i className="fa fa-circle-o" /> Morris
                      </a>
                    </li>
                    <li>
                      <a href="../charts/flot.html">
                        <i className="fa fa-circle-o" /> Flot
                      </a>
                    </li>
                    <li>
                      <a href="../charts/inline.html">
                        <i className="fa fa-circle-o" /> Inline charts
                      </a>
                    </li>
                  </ul>
                </li>
                <li className="treeview">
                  <a href="#">
                    <i className="fa fa-laptop" />
                    <span>UI Elements</span>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-left pull-right" />
                    </span>
                  </a>
                  <ul className="treeview-menu">
                    <li>
                      <a href="../UI/general.html">
                        <i className="fa fa-circle-o" /> General
                      </a>
                    </li>
                    <li>
                      <a href="../UI/icons.html">
                        <i className="fa fa-circle-o" /> Icons
                      </a>
                    </li>
                    <li>
                      <a href="../UI/buttons.html">
                        <i className="fa fa-circle-o" /> Buttons
                      </a>
                    </li>
                    <li>
                      <a href="../UI/sliders.html">
                        <i className="fa fa-circle-o" /> Sliders
                      </a>
                    </li>
                    <li>
                      <a href="../UI/timeline.html">
                        <i className="fa fa-circle-o" /> Timeline
                      </a>
                    </li>
                    <li>
                      <a href="../UI/modals.html">
                        <i className="fa fa-circle-o" /> Modals
                      </a>
                    </li>
                  </ul>
                </li>
                <li className="treeview">
                  <a href="#">
                    <i className="fa fa-edit" /> <span>Forms</span>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-left pull-right" />
                    </span>
                  </a>
                  <ul className="treeview-menu">
                    <li>
                      <a href="../forms/general.html">
                        <i className="fa fa-circle-o" /> General Elements
                      </a>
                    </li>
                    <li>
                      <a href="../forms/advanced.html">
                        <i className="fa fa-circle-o" /> Advanced Elements
                      </a>
                    </li>
                    <li>
                      <a href="../forms/editors.html">
                        <i className="fa fa-circle-o" /> Editors
                      </a>
                    </li>
                  </ul>
                </li>
                <li className="treeview">
                  <a href="#">
                    <i className="fa fa-table" /> <span>Tables</span>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-left pull-right" />
                    </span>
                  </a>
                  <ul className="treeview-menu">
                    <li>
                      <a href="../tables/simple.html">
                        <i className="fa fa-circle-o" /> Simple tables
                      </a>
                    </li>
                    <li>
                      <a href="../tables/data.html">
                        <i className="fa fa-circle-o" /> Data tables
                      </a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="../calendar.html">
                    <i className="fa fa-calendar" /> <span>Calendar</span>
                    <span className="pull-right-container">
                      <small className="label pull-right bg-red">3</small>
                      <small className="label pull-right bg-blue">17</small>
                    </span>
                  </a>
                </li>
                <li className="treeview active">
                  <a href="mailbox.html">
                    <i className="fa fa-envelope" /> <span>Mailbox</span>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-left pull-right" />
                    </span>
                  </a>
                  <ul className="treeview-menu">
                    <li className="active">
                      <a href="mailbox.html">
                        Inbox
                        <span className="pull-right-container">
                          <span className="label label-primary pull-right">
                            13
                          </span>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="compose.html">Compose</a>
                    </li>
                    <li>
                      <a href="read-mail.html">Read</a>
                    </li>
                  </ul>
                </li>
                <li className="treeview">
                  <a href="#">
                    <i className="fa fa-folder" /> <span>Examples</span>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-left pull-right" />
                    </span>
                  </a>
                  <ul className="treeview-menu">
                    <li>
                      <a href="../examples/invoice.html">
                        <i className="fa fa-circle-o" /> Invoice
                      </a>
                    </li>
                    <li>
                      <a href="../examples/profile.html">
                        <i className="fa fa-circle-o" /> Profile
                      </a>
                    </li>
                    <li>
                      <a href="../examples/login.html">
                        <i className="fa fa-circle-o" /> Login
                      </a>
                    </li>
                    <li>
                      <a href="../examples/register.html">
                        <i className="fa fa-circle-o" /> Register
                      </a>
                    </li>
                    <li>
                      <a href="../examples/lockscreen.html">
                        <i className="fa fa-circle-o" /> Lockscreen
                      </a>
                    </li>
                    <li>
                      <a href="../examples/404.html">
                        <i className="fa fa-circle-o" /> 404 Error
                      </a>
                    </li>
                    <li>
                      <a href="../examples/500.html">
                        <i className="fa fa-circle-o" /> 500 Error
                      </a>
                    </li>
                    <li>
                      <a href="../examples/blank.html">
                        <i className="fa fa-circle-o" /> Blank Page
                      </a>
                    </li>
                    <li>
                      <a href="../examples/pace.html">
                        <i className="fa fa-circle-o" /> Pace Page
                      </a>
                    </li>
                  </ul>
                </li>
                <li className="treeview">
                  <a href="#">
                    <i className="fa fa-share" /> <span>Multilevel</span>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-left pull-right" />
                    </span>
                  </a>
                  <ul className="treeview-menu">
                    <li>
                      <a href="#">
                        <i className="fa fa-circle-o" /> Level One
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="fa fa-circle-o" /> Level One
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right" />
                        </span>
                      </a>
                      <ul className="treeview-menu">
                        <li>
                          <a href="#">
                            <i className="fa fa-circle-o" /> Level Two
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="fa fa-circle-o" /> Level Two
                            <span className="pull-right-container">
                              <i className="fa fa-angle-left pull-right" />
                            </span>
                          </a>
                          <ul className="treeview-menu">
                            <li>
                              <a href="#">
                                <i className="fa fa-circle-o" /> Level Three
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <i className="fa fa-circle-o" /> Level Three
                              </a>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="#">
                        <i className="fa fa-circle-o" /> Level One
                      </a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="../../documentation/index.html">
                    <i className="fa fa-book" /> <span>Documentation</span>
                  </a>
                </li>
                <li className="header">LABELS</li>
                <li>
                  <a href="#">
                    <i className="fa fa-circle-o text-red" />{" "}
                    <span>Important</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i className="fa fa-circle-o text-yellow" />{" "}
                    <span>Warning</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i className="fa fa-circle-o text-aqua" />{" "}
                    <span>Information</span>
                  </a>
                </li>
              </ul>
            </section>
            {/* /.sidebar */}
          </aside>
          {/* Content Wrapper. Contains page content */}
          <div className="content-wrapper">
            {/* Content Header (Page header) */}
            <section className="content-header">
              <h1>
                Mailbox
                <small>13 new messages</small>
              </h1>
              <ol className="breadcrumb">
                <li>
                  <a href="#">
                    <i className="fa fa-dashboard" /> Home
                  </a>
                </li>
                <li className="active">Mailbox</li>
              </ol>
            </section>
            {/* Main content */}
            <section className="content">
              <div className="row">
                <div className="col-md-3">
                  <a
                    href="compose.html"
                    className="btn btn-primary btn-block margin-bottom"
                  >
                    Compose
                  </a>
                  <div className="box box-solid">
                    <div className="box-header with-border">
                      <h3 className="box-title">Folders</h3>
                      <div className="box-tools">
                        <button
                          type="button"
                          className="btn btn-box-tool"
                          data-widget="collapse"
                        >
                          <i className="fa fa-minus" />
                        </button>
                      </div>
                    </div>
                    <div className="box-body no-padding">
                      <ul className="nav nav-pills nav-stacked">
                        <li className="active">
                          <a href="#">
                            <i className="fa fa-inbox" /> Inbox
                            <span className="label label-primary pull-right">
                              12
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="fa fa-envelope-o" /> Sent
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="fa fa-file-text-o" /> Drafts
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="fa fa-filter" /> Junk{" "}
                            <span className="label label-warning pull-right">
                              65
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="fa fa-trash-o" /> Trash
                          </a>
                        </li>
                      </ul>
                    </div>
                    {/* /.box-body */}
                  </div>
                  {/* /. box */}
                  <div className="box box-solid">
                    <div className="box-header with-border">
                      <h3 className="box-title">Labels</h3>
                      <div className="box-tools">
                        <button
                          type="button"
                          className="btn btn-box-tool"
                          data-widget="collapse"
                        >
                          <i className="fa fa-minus" />
                        </button>
                      </div>
                    </div>
                    <div className="box-body no-padding">
                      <ul className="nav nav-pills nav-stacked">
                        <li>
                          <a href="#">
                            <i className="fa fa-circle-o text-red" /> Important
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="fa fa-circle-o text-yellow" />{" "}
                            Promotions
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="fa fa-circle-o text-light-blue" />{" "}
                            Social
                          </a>
                        </li>
                      </ul>
                    </div>
                    {/* /.box-body */}
                  </div>
                  {/* /.box */}
                </div>
                {/* /.col */}
                <div className="col-md-9">
                  <div className="box box-primary">
                    <div className="box-header with-border">
                      <h3 className="box-title">Inbox</h3>
                      <div className="box-tools pull-right">
                        <div className="has-feedback">
                          <input
                            type="text"
                            className="form-control input-sm"
                            placeholder="Search Mail"
                          />
                          <span className="glyphicon glyphicon-search form-control-feedback" />
                        </div>
                      </div>
                      {/* /.box-tools */}
                    </div>
                    {/* /.box-header */}
                    <div className="box-body no-padding">
                      <div className="mailbox-controls">
                        {/* Check all button */}
                        <button
                          type="button"
                          className="btn btn-default btn-sm checkbox-toggle"
                        >
                          <i className="fa fa-square-o" />
                        </button>
                        <div className="btn-group">
                          <button
                            type="button"
                            className="btn btn-default btn-sm"
                          >
                            <i className="fa fa-trash-o" />
                          </button>
                          <button
                            type="button"
                            className="btn btn-default btn-sm"
                          >
                            <i className="fa fa-reply" />
                          </button>
                          <button
                            type="button"
                            className="btn btn-default btn-sm"
                          >
                            <i className="fa fa-share" />
                          </button>
                        </div>
                        {/* /.btn-group */}
                        <button
                          type="button"
                          className="btn btn-default btn-sm"
                        >
                          <i className="fa fa-refresh" />
                        </button>
                        <div className="pull-right">
                          1-50/200
                          <div className="btn-group">
                            <button
                              type="button"
                              className="btn btn-default btn-sm"
                            >
                              <i className="fa fa-chevron-left" />
                            </button>
                            <button
                              type="button"
                              className="btn btn-default btn-sm"
                            >
                              <i className="fa fa-chevron-right" />
                            </button>
                          </div>
                          {/* /.btn-group */}
                        </div>
                        {/* /.pull-right */}
                      </div>
                      <div className="table-responsive mailbox-messages">
                        <table className="table table-hover table-striped">
                          <tbody>
                            <tr>
                              <td>
                                <input type="checkbox" />
                              </td>
                              <td className="mailbox-star">
                                <a href="#">
                                  <i className="fa fa-star text-yellow" />
                                </a>
                              </td>
                              <td className="mailbox-name">
                                <a href="read-mail.html">Alexander Pierce</a>
                              </td>
                              <td className="mailbox-subject">
                                <b>AdminLTE 2.0 Issue</b> - Trying to find a
                                solution to this problem...
                              </td>
                              <td className="mailbox-attachment" />
                              <td className="mailbox-date">5 mins ago</td>
                            </tr>
                            <tr>
                              <td>
                                <input type="checkbox" />
                              </td>
                              <td className="mailbox-star">
                                <a href="#">
                                  <i className="fa fa-star-o text-yellow" />
                                </a>
                              </td>
                              <td className="mailbox-name">
                                <a href="read-mail.html">Alexander Pierce</a>
                              </td>
                              <td className="mailbox-subject">
                                <b>AdminLTE 2.0 Issue</b> - Trying to find a
                                solution to this problem...
                              </td>
                              <td className="mailbox-attachment">
                                <i className="fa fa-paperclip" />
                              </td>
                              <td className="mailbox-date">28 mins ago</td>
                            </tr>
                            <tr>
                              <td>
                                <input type="checkbox" />
                              </td>
                              <td className="mailbox-star">
                                <a href="#">
                                  <i className="fa fa-star-o text-yellow" />
                                </a>
                              </td>
                              <td className="mailbox-name">
                                <a href="read-mail.html">Alexander Pierce</a>
                              </td>
                              <td className="mailbox-subject">
                                <b>AdminLTE 2.0 Issue</b> - Trying to find a
                                solution to this problem...
                              </td>
                              <td className="mailbox-attachment">
                                <i className="fa fa-paperclip" />
                              </td>
                              <td className="mailbox-date">11 hours ago</td>
                            </tr>
                            <tr>
                              <td>
                                <input type="checkbox" />
                              </td>
                              <td className="mailbox-star">
                                <a href="#">
                                  <i className="fa fa-star text-yellow" />
                                </a>
                              </td>
                              <td className="mailbox-name">
                                <a href="read-mail.html">Alexander Pierce</a>
                              </td>
                              <td className="mailbox-subject">
                                <b>AdminLTE 2.0 Issue</b> - Trying to find a
                                solution to this problem...
                              </td>
                              <td className="mailbox-attachment" />
                              <td className="mailbox-date">15 hours ago</td>
                            </tr>
                            <tr>
                              <td>
                                <input type="checkbox" />
                              </td>
                              <td className="mailbox-star">
                                <a href="#">
                                  <i className="fa fa-star text-yellow" />
                                </a>
                              </td>
                              <td className="mailbox-name">
                                <a href="read-mail.html">Alexander Pierce</a>
                              </td>
                              <td className="mailbox-subject">
                                <b>AdminLTE 2.0 Issue</b> - Trying to find a
                                solution to this problem...
                              </td>
                              <td className="mailbox-attachment">
                                <i className="fa fa-paperclip" />
                              </td>
                              <td className="mailbox-date">Yesterday</td>
                            </tr>
                            <tr>
                              <td>
                                <input type="checkbox" />
                              </td>
                              <td className="mailbox-star">
                                <a href="#">
                                  <i className="fa fa-star-o text-yellow" />
                                </a>
                              </td>
                              <td className="mailbox-name">
                                <a href="read-mail.html">Alexander Pierce</a>
                              </td>
                              <td className="mailbox-subject">
                                <b>AdminLTE 2.0 Issue</b> - Trying to find a
                                solution to this problem...
                              </td>
                              <td className="mailbox-attachment">
                                <i className="fa fa-paperclip" />
                              </td>
                              <td className="mailbox-date">2 days ago</td>
                            </tr>
                            <tr>
                              <td>
                                <input type="checkbox" />
                              </td>
                              <td className="mailbox-star">
                                <a href="#">
                                  <i className="fa fa-star-o text-yellow" />
                                </a>
                              </td>
                              <td className="mailbox-name">
                                <a href="read-mail.html">Alexander Pierce</a>
                              </td>
                              <td className="mailbox-subject">
                                <b>AdminLTE 2.0 Issue</b> - Trying to find a
                                solution to this problem...
                              </td>
                              <td className="mailbox-attachment">
                                <i className="fa fa-paperclip" />
                              </td>
                              <td className="mailbox-date">2 days ago</td>
                            </tr>
                            <tr>
                              <td>
                                <input type="checkbox" />
                              </td>
                              <td className="mailbox-star">
                                <a href="#">
                                  <i className="fa fa-star text-yellow" />
                                </a>
                              </td>
                              <td className="mailbox-name">
                                <a href="read-mail.html">Alexander Pierce</a>
                              </td>
                              <td className="mailbox-subject">
                                <b>AdminLTE 2.0 Issue</b> - Trying to find a
                                solution to this problem...
                              </td>
                              <td className="mailbox-attachment" />
                              <td className="mailbox-date">2 days ago</td>
                            </tr>
                            <tr>
                              <td>
                                <input type="checkbox" />
                              </td>
                              <td className="mailbox-star">
                                <a href="#">
                                  <i className="fa fa-star text-yellow" />
                                </a>
                              </td>
                              <td className="mailbox-name">
                                <a href="read-mail.html">Alexander Pierce</a>
                              </td>
                              <td className="mailbox-subject">
                                <b>AdminLTE 2.0 Issue</b> - Trying to find a
                                solution to this problem...
                              </td>
                              <td className="mailbox-attachment" />
                              <td className="mailbox-date">2 days ago</td>
                            </tr>
                            <tr>
                              <td>
                                <input type="checkbox" />
                              </td>
                              <td className="mailbox-star">
                                <a href="#">
                                  <i className="fa fa-star-o text-yellow" />
                                </a>
                              </td>
                              <td className="mailbox-name">
                                <a href="read-mail.html">Alexander Pierce</a>
                              </td>
                              <td className="mailbox-subject">
                                <b>AdminLTE 2.0 Issue</b> - Trying to find a
                                solution to this problem...
                              </td>
                              <td className="mailbox-attachment" />
                              <td className="mailbox-date">2 days ago</td>
                            </tr>
                            <tr>
                              <td>
                                <input type="checkbox" />
                              </td>
                              <td className="mailbox-star">
                                <a href="#">
                                  <i className="fa fa-star-o text-yellow" />
                                </a>
                              </td>
                              <td className="mailbox-name">
                                <a href="read-mail.html">Alexander Pierce</a>
                              </td>
                              <td className="mailbox-subject">
                                <b>AdminLTE 2.0 Issue</b> - Trying to find a
                                solution to this problem...
                              </td>
                              <td className="mailbox-attachment">
                                <i className="fa fa-paperclip" />
                              </td>
                              <td className="mailbox-date">4 days ago</td>
                            </tr>
                            <tr>
                              <td>
                                <input type="checkbox" />
                              </td>
                              <td className="mailbox-star">
                                <a href="#">
                                  <i className="fa fa-star text-yellow" />
                                </a>
                              </td>
                              <td className="mailbox-name">
                                <a href="read-mail.html">Alexander Pierce</a>
                              </td>
                              <td className="mailbox-subject">
                                <b>AdminLTE 2.0 Issue</b> - Trying to find a
                                solution to this problem...
                              </td>
                              <td className="mailbox-attachment" />
                              <td className="mailbox-date">12 days ago</td>
                            </tr>
                            <tr>
                              <td>
                                <input type="checkbox" />
                              </td>
                              <td className="mailbox-star">
                                <a href="#">
                                  <i className="fa fa-star-o text-yellow" />
                                </a>
                              </td>
                              <td className="mailbox-name">
                                <a href="read-mail.html">Alexander Pierce</a>
                              </td>
                              <td className="mailbox-subject">
                                <b>AdminLTE 2.0 Issue</b> - Trying to find a
                                solution to this problem...
                              </td>
                              <td className="mailbox-attachment">
                                <i className="fa fa-paperclip" />
                              </td>
                              <td className="mailbox-date">12 days ago</td>
                            </tr>
                            <tr>
                              <td>
                                <input type="checkbox" />
                              </td>
                              <td className="mailbox-star">
                                <a href="#">
                                  <i className="fa fa-star text-yellow" />
                                </a>
                              </td>
                              <td className="mailbox-name">
                                <a href="read-mail.html">Alexander Pierce</a>
                              </td>
                              <td className="mailbox-subject">
                                <b>AdminLTE 2.0 Issue</b> - Trying to find a
                                solution to this problem...
                              </td>
                              <td className="mailbox-attachment">
                                <i className="fa fa-paperclip" />
                              </td>
                              <td className="mailbox-date">14 days ago</td>
                            </tr>
                            <tr>
                              <td>
                                <input type="checkbox" />
                              </td>
                              <td className="mailbox-star">
                                <a href="#">
                                  <i className="fa fa-star text-yellow" />
                                </a>
                              </td>
                              <td className="mailbox-name">
                                <a href="read-mail.html">Alexander Pierce</a>
                              </td>
                              <td className="mailbox-subject">
                                <b>AdminLTE 2.0 Issue</b> - Trying to find a
                                solution to this problem...
                              </td>
                              <td className="mailbox-attachment">
                                <i className="fa fa-paperclip" />
                              </td>
                              <td className="mailbox-date">15 days ago</td>
                            </tr>
                          </tbody>
                        </table>
                        {/* /.table */}
                      </div>
                      {/* /.mail-box-messages */}
                    </div>
                    {/* /.box-body */}
                    <div className="box-footer no-padding">
                      <div className="mailbox-controls">
                        {/* Check all button */}
                        <button
                          type="button"
                          className="btn btn-default btn-sm checkbox-toggle"
                        >
                          <i className="fa fa-square-o" />
                        </button>
                        <div className="btn-group">
                          <button
                            type="button"
                            className="btn btn-default btn-sm"
                          >
                            <i className="fa fa-trash-o" />
                          </button>
                          <button
                            type="button"
                            className="btn btn-default btn-sm"
                          >
                            <i className="fa fa-reply" />
                          </button>
                          <button
                            type="button"
                            className="btn btn-default btn-sm"
                          >
                            <i className="fa fa-share" />
                          </button>
                        </div>
                        {/* /.btn-group */}
                        <button
                          type="button"
                          className="btn btn-default btn-sm"
                        >
                          <i className="fa fa-refresh" />
                        </button>
                        <div className="pull-right">
                          1-50/200
                          <div className="btn-group">
                            <button
                              type="button"
                              className="btn btn-default btn-sm"
                            >
                              <i className="fa fa-chevron-left" />
                            </button>
                            <button
                              type="button"
                              className="btn btn-default btn-sm"
                            >
                              <i className="fa fa-chevron-right" />
                            </button>
                          </div>
                          {/* /.btn-group */}
                        </div>
                        {/* /.pull-right */}
                      </div>
                    </div>
                  </div>
                  {/* /. box */}
                </div>
                {/* /.col */}
              </div>
              {/* /.row */}
            </section>
            {/* /.content */}
          </div>
          {/* /.content-wrapper */}
          <footer className="main-footer">
            <div className="pull-right hidden-xs">
              <b>Version</b> 2.3.6
            </div>
            <strong>
              Copyright © 2014-2016{" "}
              <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.
            </strong>{" "}
            All rights reserved.
          </footer>
          {/* Control Sidebar */}
          <aside className="control-sidebar control-sidebar-dark">
            {/* Create the tabs */}
            <ul className="nav nav-tabs nav-justified control-sidebar-tabs">
              <li>
                <a href="#control-sidebar-home-tab" data-toggle="tab">
                  <i className="fa fa-home" />
                </a>
              </li>
              <li>
                <a href="#control-sidebar-settings-tab" data-toggle="tab">
                  <i className="fa fa-gears" />
                </a>
              </li>
            </ul>
            {/* Tab panes */}
            <div className="tab-content">
              {/* Home tab content */}
              <div className="tab-pane" id="control-sidebar-home-tab">
                <h3 className="control-sidebar-heading">Recent Activity</h3>
                <ul className="control-sidebar-menu">
                  <li>
                    <a href="javascript:void(0)">
                      <i className="menu-icon fa fa-birthday-cake bg-red" />
                      <div className="menu-info">
                        <h4 className="control-sidebar-subheading">
                          Langdon's Birthday
                        </h4>
                        <p>Will be 23 on April 24th</p>
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:void(0)">
                      <i className="menu-icon fa fa-user bg-yellow" />
                      <div className="menu-info">
                        <h4 className="control-sidebar-subheading">
                          Frodo Updated His Profile
                        </h4>
                        <p>New phone +1(800)555-1234</p>
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:void(0)">
                      <i className="menu-icon fa fa-envelope-o bg-light-blue" />
                      <div className="menu-info">
                        <h4 className="control-sidebar-subheading">
                          Nora Joined Mailing List
                        </h4>
                        <p>nora@example.com</p>
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:void(0)">
                      <i className="menu-icon fa fa-file-code-o bg-green" />
                      <div className="menu-info">
                        <h4 className="control-sidebar-subheading">
                          Cron Job 254 Executed
                        </h4>
                        <p>Execution time 5 seconds</p>
                      </div>
                    </a>
                  </li>
                </ul>
                {/* /.control-sidebar-menu */}
                <h3 className="control-sidebar-heading">Tasks Progress</h3>
                <ul className="control-sidebar-menu">
                  <li>
                    <a href="javascript:void(0)">
                      <h4 className="control-sidebar-subheading">
                        Custom Template Design
                        <span className="label label-danger pull-right">
                          70%
                        </span>
                      </h4>
                      <div className="progress progress-xxs">
                        <div
                          className="progress-bar progress-bar-danger"
                          style={{ width: "70%" }}
                        />
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:void(0)">
                      <h4 className="control-sidebar-subheading">
                        Update Resume
                        <span className="label label-success pull-right">
                          95%
                        </span>
                      </h4>
                      <div className="progress progress-xxs">
                        <div
                          className="progress-bar progress-bar-success"
                          style={{ width: "95%" }}
                        />
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:void(0)">
                      <h4 className="control-sidebar-subheading">
                        Laravel Integration
                        <span className="label label-warning pull-right">
                          50%
                        </span>
                      </h4>
                      <div className="progress progress-xxs">
                        <div
                          className="progress-bar progress-bar-warning"
                          style={{ width: "50%" }}
                        />
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:void(0)">
                      <h4 className="control-sidebar-subheading">
                        Back End Framework
                        <span className="label label-primary pull-right">
                          68%
                        </span>
                      </h4>
                      <div className="progress progress-xxs">
                        <div
                          className="progress-bar progress-bar-primary"
                          style={{ width: "68%" }}
                        />
                      </div>
                    </a>
                  </li>
                </ul>
                {/* /.control-sidebar-menu */}
              </div>
              {/* /.tab-pane */}
              {/* Stats tab content */}
              <div className="tab-pane" id="control-sidebar-stats-tab">
                Stats Tab Content
              </div>
              {/* /.tab-pane */}
              {/* Settings tab content */}
              <div className="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                  <h3 className="control-sidebar-heading">General Settings</h3>
                  <div className="form-group">
                    <label className="control-sidebar-subheading">
                      Report panel usage
                      <input
                        type="checkbox"
                        className="pull-right"
                        defaultChecked
                      />
                    </label>
                    <p>Some information about this general settings option</p>
                  </div>
                  {/* /.form-group */}
                  <div className="form-group">
                    <label className="control-sidebar-subheading">
                      Allow mail redirect
                      <input
                        type="checkbox"
                        className="pull-right"
                        defaultChecked
                      />
                    </label>
                    <p>Other sets of options are available</p>
                  </div>
                  {/* /.form-group */}
                  <div className="form-group">
                    <label className="control-sidebar-subheading">
                      Expose author name in posts
                      <input
                        type="checkbox"
                        className="pull-right"
                        defaultChecked
                      />
                    </label>
                    <p>Allow the user to show his name in blog posts</p>
                  </div>
                  {/* /.form-group */}
                  <h3 className="control-sidebar-heading">Chat Settings</h3>
                  <div className="form-group">
                    <label className="control-sidebar-subheading">
                      Show me as online
                      <input
                        type="checkbox"
                        className="pull-right"
                        defaultChecked
                      />
                    </label>
                  </div>
                  {/* /.form-group */}
                  <div className="form-group">
                    <label className="control-sidebar-subheading">
                      Turn off notifications
                      <input type="checkbox" className="pull-right" />
                    </label>
                  </div>
                  {/* /.form-group */}
                  <div className="form-group">
                    <label className="control-sidebar-subheading">
                      Delete chat history
                      <a
                        href="javascript:void(0)"
                        className="text-red pull-right"
                      >
                        <i className="fa fa-trash-o" />
                      </a>
                    </label>
                  </div>
                  {/* /.form-group */}
                </form>
              </div>
              {/* /.tab-pane */}
            </div>
          </aside>
          {/* /.control-sidebar */}
          {/* Add the sidebar's background. This div must be placed
         immediately after the control sidebar */}
          <div className="control-sidebar-bg" />
        </div>
        {/* ./wrapper */}
        {/* jQuery 2.2.3 */}
        {/* Bootstrap 3.3.6 */}
        {/* Slimscroll */}
        {/* FastClick */}
        {/* AdminLTE App */}
        {/* iCheck */}
        {/* Page Script */}
        {/* AdminLTE for demo purposes */}
      </div>
    );
  },
});
