/* eslint-disable no-return-await */
/**
 * helper.js - for storing reusable logic.
 * axios.defaults.headers.common.Authorization = `Bearer ${account.tokenLogin}`; to set TOKEN default
 */
import axios from "axios";
import { logout, popupOk } from "../config";
import { showAlert, TYPE } from "common/DropdownAlert";
import i18n from "../assets/languages/i18n";
import AsyncStorageUtils from "helpers/AsyncStorageUtils";
import { hideLoading } from "common/Loading/LoadingModal";

axios.defaults.timeout = 10000;
export const token_MOB = "74798b2e-952d-4d9b-9d59-a08822a333be";
export const token_WEB = "28f4b84b-79e8-4f18-a4cc-acfbbb47dd3f";

/**
 *
 * @param {*} url is link api
 * @param {*} isAuth is state auth
 */
export async function GetData(url, data) {
  let token = await AsyncStorageUtils.get(
    AsyncStorageUtils.KEY.SMART_OFFICE_TOKEN
  );
  let tokenVtf = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.VTF_TOKEN);
  let myRequest = {
    method: "get",
    url,
    headers: {
      "X-Gravitee-Api-Key": token_MOB,
      "api-key": token,
      "vtf-key": tokenVtf,
    },
    params: {
      ...data,
    },
    timeout: 60 * 1000,
    withCredentials: true,
  };

  console.log("get data", myRequest);
  return await axios(myRequest)
    .then((response) => response)
    .then((response) => response)
    .catch((error) => {
      checkSessionEnd(error);
      console.log(error.request);
      error;
    });
}

export async function GetDataVtfToken(url, data) {
  let tokenVtf = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.VTF_TOKEN);
  let token = await AsyncStorageUtils.get(
    AsyncStorageUtils.KEY.SMART_OFFICE_TOKEN
  );
  let myRequest = {
    method: "get",
    url,
    headers: {
      "X-Gravitee-Api-Key": token_MOB,
      "vtf-key": tokenVtf,
      "api-key": token,
    },
    params: {
      ...data,
    },
    timeout: 60 * 1000,
    withCredentials: true,
  };

  console.log("get data", myRequest);
  return await axios(myRequest)
    .then((response) => response)
    .then((response) => response)
    .catch((error) => {
      checkSessionEnd(error);
      error;
    });
}

export async function GetAvatarData(url, data) {
  let token = await AsyncStorageUtils.get(
    AsyncStorageUtils.KEY.SMART_OFFICE_TOKEN
  );
  let tokenVtf = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.VTF_TOKEN);
  let myRequest = {
    method: "get",
    url,
    headers: {
      "X-Gravitee-Api-Key": "06b932f2-b6ba-4e4c-97c4-2132628956ef",
      "api-key": token,
      "vtf-key": tokenVtf,
    },
    params: {
      ...data,
    },
    timeout: 10 * 1000,
    withCredentials: true,
  };

  console.log("get data", myRequest);
  return await axios(myRequest)
    .then((response) => response)
    .then((response) => response)
    .catch((error) => {
      checkSessionEnd(error);
      error;
    });
}

// Web
export async function GetData_WEB(url, data) {
  let token = await AsyncStorageUtils.get(
    AsyncStorageUtils.KEY.SMART_OFFICE_TOKEN
  );
  let tokenVtf = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.VTF_TOKEN);
  let myRequest = {
    method: "get",
    url,
    headers: {
      "X-Gravitee-Api-Key": token_WEB,
      "api-key": token,
      "vtf-key": tokenVtf,
    },
    params: {
      data,
    },
    timeout: 60 * 1000,
    withCredentials: true,
  };
  console.log("get data web", myRequest);
  return await axios(myRequest)
    .then((response) => response)
    .then((response) => response)
    .catch((error) => {
      checkSessionEnd(error);
      error;
    });
}
export async function GetDataREQUEST(url, data) {
  let tokenVtf = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.VTF_TOKEN);
  let token = await AsyncStorageUtils.get(
    AsyncStorageUtils.KEY.SMART_OFFICE_TOKEN
  );

  let myRequest = {
    method: "get",
    url,
    headers: {
      "X-Gravitee-Api-Key": token_MOB,
      "vtf-key": tokenVtf,
      "api-key": token,
    },
    timeout: 60 * 1000,
    withCredentials: true,
  };
  //await AsyncStorageUtils.save(AsyncStorageUtils.KEY.VTF_TOKEN,vtfToken ?? '')
  console.log("get wwwwwwwwww", myRequest);
  return await axios(myRequest)
    .then((response) => response)
    .then((response) => response)
    .catch((error) => {
      error;
    });
}

export async function GetData_WEBVtfToken(url, data) {
  let tokenVtf = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.VTF_TOKEN);
  let token = await AsyncStorageUtils.get(
    AsyncStorageUtils.KEY.SMART_OFFICE_TOKEN
  );
  let myRequest = {
    method: "get",
    url,
    headers: {
      "X-Gravitee-Api-Key": token_WEB,
      "vtf-key": tokenVtf,
      "api-key": token,
    },
    params: {
      data,
    },
    timeout: 60 * 1000,
    withCredentials: true,
  };
  console.log("get data web", myRequest);
  return await axios(myRequest)
    .then((response) => response)
    .then((response) => response)
    .catch((error) => {
      checkSessionEnd(error);
      error;
    });
}
/**
 *
 * @param {*} url is link api
 * @param {*} json is input format json to request server
 * @param {*} isAuth is state auth
 */
export async function PostData(url, json, isAuth = true) {
  let token = await AsyncStorageUtils.get(
    AsyncStorageUtils.KEY.SMART_OFFICE_TOKEN
  );
  let tokenVtf = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.VTF_TOKEN);
  let myRequest = {
    method: "post",
    url,
    headers: {
      "Content-Type": "application/json",
      "X-Gravitee-Api-Key": token_MOB,
      "api-key": token,
      "vtf-key": tokenVtf,
    },
    timeout: 60 * 1000,
    data: JSON.stringify(json),
    withCredentials: true,
  };
  console.log("post data mobile", myRequest);
  return await axios(myRequest)
    .then((response) => response)
    .then((response) => response)
    .catch((error) => {
      checkSessionEnd(error);
      if (error.response.data.returnMessage) {
        showAlert(
          TYPE.ERROR,
          i18n.t("NOTICE_T"),
          error.response.data.returnMessage
        );
      }
      console.log(error.request);

      error;
    });
}

/**
 *
 * @param {*} url is link api
 * @param {*} json is input format json to request server
 * @param {*} isAuth is state auth
 */
export async function PostDataVTFToken(url, json, isAuth = true) {
  let tokenVtf = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.VTF_TOKEN);
  let token = await AsyncStorageUtils.get(
    AsyncStorageUtils.KEY.SMART_OFFICE_TOKEN
  );
  let myRequest = {
    method: "post",
    url,
    headers: {
      "Content-Type": "application/json",
      "X-Gravitee-Api-Key": token_MOB,
      "vtf-key": tokenVtf,
      "api-key": token,
    },
    timeout: 60 * 1000,
    data: JSON.stringify(json),
    withCredentials: true,
  };
  console.log("post data mobile", myRequest);
  return await axios(myRequest)
    .then((response) => response)
    .then((response) => response)
    .catch((error) => {
      checkSessionEnd(error);
      if (error.response.data.returnMessage) {
        showAlert(
          TYPE.ERROR,
          i18n.t("NOTICE_T"),
          error.response.data.returnMessage
        );
      }
      error;
    });
}

// Post Web
export async function PostData_WEB(url, json, isAuth = true) {
  let token = await AsyncStorageUtils.get(
    AsyncStorageUtils.KEY.SMART_OFFICE_TOKEN
  );
  let tokenVtf = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.VTF_TOKEN);
  let myRequest = {
    method: "post",
    url,
    headers: {
      "Content-Type": "application/json",
      "X-Gravitee-Api-Key": token_WEB,
      "api-key": token,
      "vtf-key": tokenVtf,
    },
    timeout: 60 * 1000,
    data: JSON.stringify(json),
    withCredentials: true,
  };
  console.log("PostData_WEB", myRequest);
  return await axios(myRequest)
    .then((response) => response)
    .then((response) => response)
    .catch((error) => {
      checkSessionEnd(error);
      if (error.response.data.returnMessage) {
        showAlert(
          TYPE.ERROR,
          i18n.t("NOTICE_T"),
          error.response.data.returnMessage
        );
      }
      error;
    });
}

/**
 *
 * @param {*} url is link api
 * @param {*} json is input format json to request server
 * @param {*} isAuth is state auth
 */
export async function PutData(url, json, isAuth = true) {
  let token = await AsyncStorageUtils.get(
    AsyncStorageUtils.KEY.SMART_OFFICE_TOKEN
  );
  let tokenVtf = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.VTF_TOKEN);
  let myRequest = {
    method: "put",
    url,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "X-Gravitee-Api-Key": token_MOB,
      "api-key": token,
      "vtf-key": tokenVtf,
    },
    data: JSON.stringify(json),
    withCredentials: true,
  };
  console.log("PutData", myRequest);
  return await axios(myRequest)
    .then((response) => response)
    .then((response) => response)
    .catch((error) => {
      checkSessionEnd(error);
      error;
    });
}

//PUT WEB
export async function PutData_WEB(url, json, isAuth = true) {
  let token = await AsyncStorageUtils.get(
    AsyncStorageUtils.KEY.SMART_OFFICE_TOKEN
  );
  let tokenVtf = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.VTF_TOKEN);
  let myRequest = {
    method: "put",
    url,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "X-Gravitee-Api-Key": token_WEB,
      "api-key": token,
      "vtf-key": tokenVtf,
    },
    data: JSON.stringify(json),
    withCredentials: true,
  };
  return await axios(myRequest)
    .then((response) => response)
    .then((response) => response)
    .catch((error) => {
      checkSessionEnd(error);
      error;
    });
}

/**
 *
 * @param {*} url is link api
 * @param {*} isAuth is state auth
 */
export async function DelData(url, isAuth = true) {
  let token = await AsyncStorageUtils.get(
    AsyncStorageUtils.KEY.SMART_OFFICE_TOKEN
  );
  let tokenVtf = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.VTF_TOKEN);
  let myRequest = {
    headers: {
      "Content-Type": "application/json",
      "X-Gravitee-Api-Key": token_MOB,
      "api-key": token,
      "vtf-key": tokenVtf,
    },
    timeout: 60 * 1000,
    withCredentials: true,
  };
  console.log("deldata", myRequest, url);
  return await axios
    .delete(url, myRequest)
    .then((response) => response)
    .then((response) => response)
    .catch((error) => {
      checkSessionEnd(error);
      error;
    });
}

/**
 *
 * @param {*} url is link api
 * @param {*} json is input format json to request server
 * @param {*} isAuth is state auth
 */
export async function PostFormData(url, json, isAuth = true) {
  let token = await AsyncStorageUtils.get(
    AsyncStorageUtils.KEY.SMART_OFFICE_TOKEN
  );
  let tokenVtf = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.VTF_TOKEN);
  let myRequest = {
    method: "post",
    url,
    headers: {
      "Content-Type": "multipart/form-data",
      "X-Gravitee-Api-Key": token_MOB,
      "api-key": token,
      "vtf-key": tokenVtf,
    },
    data: json,
    withCredentials: true,
    timeout: 180 * 1000,
  };
  console.log("create vOffice from print", myRequest);
  return await axios(myRequest)
    .then((response) => response)
    .then((response) => response)
    .catch((error) => {
      checkSessionEnd(error);
      popupOk(i18n.t("NOTICE_T"), error.response.data.error.message);
    });
}

/**
 *
 * @param {*} url is link api
 * @param {*} json is input format json to request server
 * @param {*} isAuth is state auth
 */
export async function PostFormDataPrint(url, password, json, isAuth = true) {
  let token = await AsyncStorageUtils.get(
    AsyncStorageUtils.KEY.SMART_OFFICE_TOKEN
  );
  let tokenVtf = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.VTF_TOKEN);
  let myRequest = {
    method: "post",
    url,
    headers: {
      "Content-Type": "multipart/form-data",
      "X-Gravitee-Api-Key": token_MOB,
      "api-key": token,
      "vtf-key": tokenVtf,
    },
    params: {
      ...password,
    },
    data: json,
    withCredentials: true,
    timeout: 180 * 1000,
  };
  return await axios(myRequest)
    .then((response) => response)
    .then((response) => response)
    .catch((error) => {
      checkSessionEnd(error);
      popupOk(i18n.t("NOTICE_T"), error.response.data.error.message);
    });
}

/**
 *
 * @param {*} url is link api
 * @param {*} json is input format json to request server
 * @param {*} isAuth is state auth
 */
export async function PutFormData(url, json, isAuth = true) {
  let token = await AsyncStorageUtils.get(
    AsyncStorageUtils.KEY.SMART_OFFICE_TOKEN
  );
  let tokenVtf = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.VTF_TOKEN);
  let myRequest = {
    method: "put",
    url,
    headers: {
      "Content-Type": "multipart/form-data",
      "api-key": token,
      "vtf-key": tokenVtf,
    },
    data: json,
    withCredentials: true,
  };
  return await axios(myRequest)
    .then((response) => response)
    .then((response) => response)
    .catch((error) => {
      checkSessionEnd(error);
      popupOk(i18n.t("NOTICE_T"), error.response.data.error.message);
    });
}

export function checkSessionEnd(error) {
  if (error.request.status === 401) {
    console.log("authen----", error.request);
    hideLoading();
    showAlert(TYPE.ERROR, i18n.t("NOTICE_T"), i18n.t("LOGIN_SESSION_END"));
    setTimeout(() => logout(), 1000);
  }
}

export async function generateHeader(isWeb = false) {
  let token = await AsyncStorageUtils.get(
    AsyncStorageUtils.KEY.SMART_OFFICE_TOKEN
  );
  let tokenVtf = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.VTF_TOKEN);
  return {
    "X-Gravitee-Api-Key": isWeb ? token_WEB : token_MOB,
    "api-key": token,
    "vtf-key": tokenVtf,
  };
}
