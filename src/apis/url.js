import { NetworkSetting } from "../config/index";
export default {
  CREATE_INVOICE: `${NetworkSetting.ROOT}/apInvoiceServiceRest/apInvoice/`,
  EDIT_INVOICE: `${NetworkSetting.ROOT}/apInvoiceServiceRest/apInvoice/update`,
};
