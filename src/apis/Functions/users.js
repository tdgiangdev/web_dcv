/* eslint-disable handle-callback-err */
import { PostData, GetData } from "../helpers";
import url from "apis/url";

export const saveDeviceToken = async (body) =>
  PostData(url.SAVE_DEVICE_TOKEN_REQUEST, body)
    .then((res) => res)
    .catch((err) => null);

export const getUser = (id) =>
  GetData(`${url.GET_USER_REQUEST}${id}`, {})
    .then((res) => res)
    .catch((err) => err);
